#!/bin/bash

set -e

DOC_DIR="/data/documents"
DROPBOX_DIR="/root/Dropbox"
BACKUP_DIR="/backup"


FILE=""
VERBOSE=0
HELP=0
REMOVE=0

function help() {
    echo ""
    echo "Usage ./ce-archive-commands (-rm)"
    echo ""
    echo " -rm,--remove        Remove a file from the ce archive and dropbox"
    echo " -v, --verbose       Ouput all commands run by the script"
    echo " -h, --help          Show this help"
    echo ""
}

function remove() {
    echo "Removing ${FILE}"

    if [ -f "${DOC_DIR}/${FILE}" ]; then
        cp "${DOC_DIR}/${FILE}" "${BACKUP_DIR}/${FILE}.local"
        rm "${DOC_DIR}/${FILE}"
    else
        echo "Local file ${DOC_DIR}/${FILE} not found"
    fi

    if [ -f "${DROPBOX_DIR}/CLARIN-EU/CE_Documents/${FILE}" ]; then
        cp "${DROPBOX_DIR}/CLARIN-EU/CE_Documents/${FILE}" "${BACKUP_DIR}/${FILE}.remote"
        rm "${DROPBOX_DIR}/CLARIN-EU/CE_Documents/${FILE}"
    else
        echo "Remote file ${DROPBOX_DIR}/CLARIN-EU/CE_Documents/${FILE} not found"
    fi
}

if [[ $# -eq 0 ]]; then
    echo ""
    echo "[ERROR] Argument(s) required"
    help
    exit
fi

#Process script arguments
while [[ $# -gt 0 ]]
do
key="$1"
case $key in
    -rm|--remove)
        REMOVE=1
        FILE=$2
        shift
        ;;
    -v|--verbose)
        VERBOSE=1
        ;;
    -h|--help)
        HELP=1
        ;;
    *)
        echo ""
        echo "[ERROR] Unkown option: $key"
        HELP=1
        ;;
esac
shift # past argument or value
done

if [ "${VERBOSE}" -eq 1 ]; then
    set -x
fi

if [ "${HELP}" -eq 1 ]; then
    help
    exit
fi

if [ "${REMOVE}" -eq 1 ]; then
    remove
fi
